{
  "_id": "lLsRXE3PooNQvEBX",
  "name": "Mummified Creature",
  "type": "feat",
  "img": "systems/pf1/icons/skills/violet_07.jpg",
  "sort": 0,
  "flags": {},
  "system": {
    "description": {
      "value": "<p><strong>Acquired/Inherited Template</strong> Acquired<br><strong>Simple Template</strong> No<br><strong>Usable with Summons</strong> No<p>Many ancient cultures mummify their dead, preserving the bodies of the deceased through lengthy and complex funerary and embalming processes. While the vast majority of these corpses are mummified simply to preserve the bodies in the tombs where they are interred, some are mummified with the help of magic to live on after death as mummified creatures. A mummified creature appears much as other mummies do—a dusty corpse, desiccated and withered, swathed in a funeral shroud of linen wrappings adorned with hieroglyphs—but a spark of malign intelligence gleams in its unliving eyes.<p>Mummified creatures differ from the standard mummy presented in the Pathfinder RPG Bestiary with regard to how and why they are created. Most standard mummies are created as simple tomb guardians; they gain abilities such as an aura of despair and mummy rot, but they usually lose their free will, much of their intelligence, and the abilities they possessed in life. A mummified creature, on the other hand, retains its intelligence, memories, and many of its other abilities. A mummified creature does not spread the curse of mummy rot, nor does the sight of it paralyze the living with fear, but its touch can reduce a living creature to dust and its very presence is frightening. Though slow and clumsy in undeath, a mummified creature is nonetheless capable of surprising bursts of speed and ferocity. Because of its creation process, however, a mummified creature is susceptible to energy damage, though determining an individual mummified creature’s vulnerability is not always easy.<p>Many mummified creatures are created to guard the tombs of important figures, but some powerful beings—rulers, high priests, mighty wizards, or even wealthy aristocrats—arrange to be transformed into mummified creatures upon their deaths. Unwilling to give up their lives and knowledge to the whims of fate, these people bind their souls to the dried husks of their dead bodies, trading oblivion for endless centuries of unlife. The truly wealthy sometimes arrange for their most favored spouses, concubines, servants, or guards to be mummified with them, enabling them to hold court in dusty tombs in an undead mockery of their old lives centuries after they perished.<p>To create a mummified creature, a corpse must be prepared through embalming, with its internal organs replaced with dried herbs and flowers and its dead skin preserved through the application of sacred oils. Unlike with standard mummies, a mummified creature’s brain is not removed from its skull after death. Injected with strange chemicals and tattooed with mystical hieroglyphs, a mummified creature’s brain retains the base creature’s mind and abilities, though the process does result in the loss of some mental faculties. Once this process is complete, the body is wrapped in special purified linens marked with hieroglyphs that grant the mummified creature its new abilities (as well as its weakness). Finally, the creator must cast a create greater undead spell to give the mummified creature its unlife.<p>“Mummified creature” is an acquired template that can be added to any living corporeal creature (hereafter referred to as the base creature). A mummified creature uses all of the base creature’s statistics except as noted here.<p><strong>CR:</strong> Same as the base creature +1.</p>\n<p><strong>Alignment:</strong> Any evil.</p>\n<p><strong>Type: </strong>The creature’s type changes to undead (augmented). It retains any other subtypes as well, except for alignment subtypes and subtypes that indicate kind. Do not recalculate class HD, BAB, saves, or skill points.</p>\n<p><strong>Senses: </strong>A mummified creature gains darkvision 60 feet.</p>\n<p><strong>Aura: </strong>A mummified creature gains a frightful presence aura with a range of 30 feet and a duration of 1d6 rounds.</p>\n<p><strong>Armor Class: </strong>Natural armor improves by +4.</p>\n<p><strong>Hit Dice: </strong>Change all racial Hit Dice to d8s. Class Hit Dice are unaffected. As an undead, a mummified creature uses its Charisma modifier to determine bonus hit points (instead of Constitution).</p>\n<p><strong>Defensive Abilities: </strong>A mummified creature gains DR 5/— and the defensive abilities granted by the undead type.</p>\n<p><strong>Weaknesses: </strong>The mummification process leaves a mummified creature vulnerable to a single energy type. Choose or determine randomly from the following list.</p>\n<hr>\n<table>\n<tbody>\n<tr>\n<td><strong>d10</strong></td>\n<td><strong>Energy</strong></td>\n</tr>\n<tr>\n<td>1</td>\n<td>Electricity</td>\n</tr>\n<tr>\n<td>2–3</td>\n<td>Acid</td>\n</tr>\n<tr>\n<td>4–7</td>\n<td>Fire</td>\n</tr>\n<tr>\n<td>8–9</td>\n<td>Cold</td>\n</tr>\n<tr>\n<td>10</td>\n<td>Sonic</td>\n</tr>\n</tbody>\n</table>\n<hr>\n<p><br>As a fail-safe in case of rebellion, a mummified creature is subtly marked during the ritual process with a hieroglyph someplace inconspicuous on its body or wrappings that identifies the particular energy type to which it is vulnerable. A successful DC 20 Perception check is needed to find the mark, but a successful DC 25 Linguistics check is still required to decipher the hieroglyph’s meaning.</p>\n<p><strong>Speed: </strong>Decrease all speeds by 10 feet (to a minimum of 5 feet). If the base creature has a flight speed, its maneuverability changes to clumsy.</p>\n<p><strong>Attacks: </strong>The mummification process hardens the mummified creature’s bones to a stone-like density, granting the mummif ied creature a powerful slam attack if the base creature has no other natural attacks. This slam attack deals damage based on the mummified creature’s size (Bestiary 302), treating the creature as if it were one size category larger.</p>\n<p><strong>Special Attacks: </strong>A mummified creature gains the following special attacks.<p><em>Burst of Vengeance (Su): </em>Despite its slow, lumbering nature, a mummified creature is capable of lurching forward to attack with a short but surprising explosion of speed. Twice per day as a swift action, a mummified creature may act as if affected by a haste spell for 1 round.<p><em>Dust Stroke (Su): </em>A creature killed by a mummified creature’s natural attack or slam attack is disintegrated into a cloud of dust and ash, completely destroying the victim’s body (as disintegrate).<p><strong>Abilities: </strong>Str +4, Int –2 (minimum 1). As an undead creature, a mummified creature has no Constitution score.</p>\n<p><strong>Feats: </strong>A mummified creature gains Toughness as a bonus feat, and Improved Natural Attack as a bonus feat for each of the base creature’s natural attacks.</p>\n<p><strong>Skills: </strong>A mummified creature gains a +4 racial bonus on Stealth checks.</p>"
    },
    "tags": [],
    "actions": [],
    "attackNotes": [],
    "effectNotes": [],
    "changes": [
      {
        "_id": "3y04cfnb",
        "formula": "4",
        "operator": "add",
        "subTarget": "nac",
        "modifier": "untyped",
        "priority": 0,
        "value": 0,
        "target": "ac"
      },
      {
        "_id": "us2vzwj3",
        "formula": "4",
        "operator": "add",
        "subTarget": "str",
        "modifier": "untyped",
        "priority": 0,
        "value": 0,
        "target": "ability"
      },
      {
        "_id": "1od5741y",
        "formula": "-2",
        "operator": "add",
        "subTarget": "int",
        "modifier": "untyped",
        "priority": 0,
        "value": 0,
        "target": "ability"
      },
      {
        "_id": "7et4dqnx",
        "formula": "4",
        "operator": "add",
        "subTarget": "skill.ste",
        "modifier": "racial",
        "priority": 0,
        "value": 0,
        "target": "skill"
      }
    ],
    "contextNotes": [],
    "links": {
      "children": [],
      "charges": []
    },
    "armorProf": {
      "value": []
    },
    "weaponProf": {
      "value": []
    },
    "languages": {
      "value": []
    },
    "scriptCalls": [],
    "subType": "template",
    "associations": {
      "classes": []
    },
    "crOffset": "1"
  }
}
