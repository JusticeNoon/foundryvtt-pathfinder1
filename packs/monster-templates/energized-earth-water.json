{
  "_id": "7NmE4uSQYw3nsRAN",
  "name": "Energized (Earth, Water)",
  "type": "feat",
  "img": "systems/pf1/icons/skills/violet_07.jpg",
  "sort": 0,
  "flags": {},
  "system": {
    "description": {
      "value": "<p><strong>Acquired/Inherited Template</strong> Acquired<br><strong>Simple Template</strong> No<br><strong>Usable with Summons</strong> No<p>Most golems are animated by an elemental spirit bound within a constructed body, but some creators build their golems with a greater purpose in mind. An energized golem is infused with the raw elemental energy of the elemental spirit used to animate it, granting it increased strength and agility and a host of supernatural powers.<p>“Energized” is an acquired template that can be added to any golem (referred to hereafter as the base creature). An energized golem uses all the base creature’s statistics and special abilities except as noted here. Save DCs are equal to 10 + half the energized golem’s Hit Dice + the energized golem’s Constitution modifier.<p><strong>Challenge Rating:</strong> Base creature’s CR + 2.<p><em>Aura (Su): </em>An energized golem radiates one of the elemental energy auras described below. All creatures within 5 feet of the energized golem take 1d6 points of energy damage at the beginning of the golem’s turn. Each type of elemental aura generates an additional effect within this range, as is described below. For every 10 Hit Dice the energized golem has, the range of the aura extends by 5 feet and it deals an additional 1d6 points of energy damage. The type of energy damage and any additional effects of the aura are determined by the elemental overcharge special quality.<p><em>Blizzard:</em> The frigid temperatures surrounding the energized golem deal an additional amount of nonlethal damage equal to the damage dice of its elemental aura to all creatures within range each round. A creature can attempt a Fortitude save to negate this additional nonlethal damage. Creatures that take any amount of nonlethal damage from this effect are fatigued. If a target is already fatigued, it is instead exhausted. The fatigued or exhaustion condition persists until the creature recovers from the nonlethal damage.<p><em>Caustic Mist:</em> A poisonous miasma emanates from the energized golem, afflicting those within range each round with a deadly toxin that rapidly destroys flesh, muscle, and organs alike. Poison (Ex): Aura—inhaled; save Fort; frequency 1/round for 6 rounds; effect 1 Str, 1 Dex, and 1 Con damage; cure 2 saves.<p><em>Immolation:</em> The extreme heat surrounding the energized golem causes each creature and unattended object within range to catch fire unless it succeeds at a Reflex save. Each affected creature or item takes an additional amount of fire damage equal to the number of damage dice for the energized golem’s elemental aura immediately and each round thereafter as long as it remains within the aura. A creature or object that has caught on fire but moves outside of the aura instead takes 1d6 points of fire damage each round and can attempt another Reflex save each round to extinguish the flames.<p><em>Swirling Winds:</em> Powerful winds surround the energized golem, buffeting creatures within range and dealing an additional amount of bludgeoning damage equal to the damage dice of its elemental aura. These winds otherwise function as a <a href=\"https://aonprd.com/SpellDisplay.aspx?ItemName=gust%20of%20wind\">gust of wind</a> spell. An affected creature can attempt a Reflex save to negate this additional damage, but a successful Fortitude save is required to negate the <a href=\"https://aonprd.com/SpellDisplay.aspx?ItemName=gust%20of%20wind\">gust of wind</a> effect. If the energized golem has 10 or more Hit Dice, the DC of Fly or Strength checks to resist the effects of the winds increases by 5.<p><strong>Armor Class: </strong>Natural armor improves by 3.<p><strong>Hit Points:</strong> An energized golem receives double the bonus hit points based on its size granted by the construct creature type.<p><strong>Defensive Abilities:</strong> An energized golem’s body is infused with elemental energy, granting it immunity to a single energy type. Refer to the elemental overcharge special quality below.<p><strong>Special Attacks: </strong>An energized golem retains all of the base creature’s special abilities and gains the following special attack.<p><em>Energy Discharge (Su):</em> Once per hour, an energized golem can discharge a pulse of energy from its body as a standard action, affecting all targets within a 20-foot burst. This burst deals 1d8 points of energy damage for every 2 Hit Dice the energized golem has (Reflex half ). Each creature damaged by this ability must succeed at a second saving throw or suffer an additional effect. The type of energy damage, the additional effect, and the type of save to avoid this additional effect are determined by the elemental overcharge special quality.<p><strong>Ability Scores: </strong>An energized golem gains ability scores based on the base creature’s Hit Dice and the type of elemental spirit used in its creation. If the elemental spirit used is an air or fire elemental spirit, the energized golem gains a +2 bonus to Strength and +4 bonus to Dexterity. If the elemental spirit used is an earth or water elemental spirit, the energized golem gains a +4 bonus to Strength and +2 bonus to Dexterity. If the base creature has 10 or more Hit Dice, these ability score bonuses increase to +8 and +4.<p><strong>Special Qualities: </strong>An energized golem retains all of the base creature’s special qualities and gains the following special quality.<p><em>Elemental Overcharge (Su):</em> An energized golem is augmented by the elemental spirit that is bound to it during its creation. Its natural attacks deal 1d6 points of energy damage for every 6 Hit Dice the golem has. The type of damage dealt, the golem’s immunity, and its aura are based on the elemental spirit bound to the golem during its creation, as listed on the table below.<br><br></p>\n<table>\n<tbody>\n<tr>\n<td><strong>Elemental</strong></td>\n<td><strong>Energy</strong></td>\n<td><strong>Aura</strong></td>\n<td><strong>Discharge Effect (Saving Throw Type)</strong></td>\n</tr>\n<tr>\n<td>Air</td>\n<td>Electricity</td>\n<td>Swirling winds</td>\n<td>Stunned for 1 round (Fortitude)</td>\n</tr>\n<tr>\n<td>Earth</td>\n<td>Acid</td>\n<td>Caustic mist</td>\n<td>Acid clings to targets, dealing half damage next round (Reflex)</td>\n</tr>\n<tr>\n<td>Fire</td>\n<td>Fire</td>\n<td>Immolation</td>\n<td>Knocked prone (Fortitude)</td>\n</tr>\n<tr>\n<td>Water</td>\n<td>Cold</td>\n<td>Blizzard</td>\n<td>Entangled for 1d4+1 rounds (Reflex)</td>\n</tr>\n</tbody>\n</table>"
    },
    "tags": [],
    "actions": [],
    "attackNotes": [],
    "effectNotes": [],
    "changes": [
      {
        "_id": "8xddbabx",
        "formula": "3",
        "operator": "add",
        "subTarget": "nac",
        "modifier": "untyped",
        "priority": 0,
        "value": 0,
        "target": "ac"
      },
      {
        "_id": "mxxfzrms",
        "formula": "@attributes.hd.total > 9 ? 4 : 2",
        "operator": "add",
        "subTarget": "dex",
        "modifier": "untyped",
        "priority": 0,
        "value": 0,
        "target": "ability"
      },
      {
        "_id": "sbap02gv",
        "formula": "@attributes.hd.total > 9 ? 8 : 4",
        "operator": "add",
        "subTarget": "str",
        "modifier": "untyped",
        "priority": 0,
        "value": 0,
        "target": "ability"
      }
    ],
    "contextNotes": [],
    "links": {
      "children": [],
      "charges": []
    },
    "armorProf": {
      "value": []
    },
    "weaponProf": {
      "value": []
    },
    "languages": {
      "value": []
    },
    "scriptCalls": [],
    "subType": "template",
    "associations": {
      "classes": []
    },
    "crOffset": "2"
  }
}
