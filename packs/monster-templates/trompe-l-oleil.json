{
  "_id": "es3RMR9PqdUJfoxn",
  "name": "Trompe l'Oleil",
  "type": "feat",
  "img": "systems/pf1/icons/skills/violet_07.jpg",
  "sort": 0,
  "flags": {},
  "system": {
    "description": {
      "value": "<p><strong>Acquired/Inherited Template </strong>Inherited<br><strong>Simple Template </strong>No<br><strong>Usable with Summons</strong> No<p>Trompe l’oeil creatures are life-sized portraits animated by powerful magic or occult phenomena. Able to move and talk, these constructs can also step out of their frames to become three-dimensional beings. Born from artistic masterpieces, trompe l’oeils can easily pass for their original models, though close examination reveals that they are not flesh and blood, but only layers of paint. Trompe l’oeils can be created to act as guardians and spies, or on occasion, a painting will animate spontaneously. Rarely, a portrait is so lifelike, a nascent spirit is able to inhabit it. Believing itself to be as good as or better than the original, such a trompe l’oeil seeks to eliminate and replace the painting’s subject.<p>“Trompe l’oeil” is an inherited template that can be added to any corporeal creature that has an Intelligence score (referred to hereafter as the base creature).<p><strong>Challenge Rating:</strong> Base creature’s CR + 1.<p><strong>Alignment:</strong> A trompe l’oeil usually has the same alignment as its creator or the base creature. A trompe l’oeil that seeks to destroy its original model, however, has an evil alignment (but the same alignment on the chaotic/lawful axis).<p><strong>Type: </strong>The creature’s type changes to construct. Do not recalculate BAB, saves, or skill ranks.<p><strong>Armor Class:</strong> A trompe l’oeil gains a bonus to AC based on its HD, as noted in the following table. If it is depicted wearing armor or a shield, these items are masterwork and gain an enhancement bonus (or equivalent armor special abilities) when worn by the trompe l’oeil, as indicated in the table. If the trompe l’oeil is depicted without armor, add the armor enhancement bonus to its natural armor bonus instead. Armor and shields equipped by a trompe l’oeil melt into puddles of nonmagical paint when the creature is destroyed.</p>\n<hr>\n<table>\n<tbody>\n<tr>\n<td><strong>Trompe l'Oeil HD</strong></td>\n<td><strong>Armor Enhancement Bonus</strong></td>\n<td><strong>Shield Enhancement Bonus</strong></td>\n</tr>\n<tr>\n<td>1-4</td>\n<td>—</td>\n<td>—</td>\n</tr>\n<tr>\n<td>5-8</td>\n<td>+1</td>\n<td>—</td>\n</tr>\n<tr>\n<td>9-12</td>\n<td>+2</td>\n<td>+1</td>\n</tr>\n<tr>\n<td>13-16</td>\n<td>+3</td>\n<td>+1</td>\n</tr>\n<tr>\n<td>17+</td>\n<td>+4</td>\n<td>+2</td>\n</tr>\n</tbody>\n</table>\n<p><br><strong>Hit Dice: </strong>Change all of the creature’s racial Hit Dice to d10s. All Hit Dice derived from class levels remain unchanged. As constructs, trompe l’oeils gain a number of additional hit points as noted in the following table.</p>\n<hr>\n<table>\n<tbody>\n<tr>\n<td><strong>Tromp l'Oeil Size</strong></td>\n<td><strong>Bonus Hit Points</strong></td>\n</tr>\n<tr>\n<td>Tiny or smaller</td>\n<td>—</td>\n</tr>\n<tr>\n<td>Small</td>\n<td>+10</td>\n</tr>\n<tr>\n<td>Medium</td>\n<td>+20</td>\n</tr>\n<tr>\n<td>Large</td>\n<td>+30</td>\n</tr>\n<tr>\n<td>Huge</td>\n<td>+40</td>\n</tr>\n<tr>\n<td>Gargantuan</td>\n<td>+60</td>\n</tr>\n<tr>\n<td>Colossal</td>\n<td>+80</td>\n</tr>\n</tbody>\n</table>\n<p><br><strong>Defensive Abilities:</strong> A trompe l’oeil gains the standard immunities and traits of construct creatures. In addition, it gains rejuvenation.<p><em>Rejuvenation (Su):</em> When a trompe l’oeil is destroyed, it reforms 2d4 days later on its original canvas (see page 243). The only way to permanently destroy a trompe l’oeil is to destroy the original canvas before the creature reforms.<p><strong>Attacks:</strong> A trompe l’oeil retains all weapon proficiencies and natural weapons. If it’s depicted wielding any manufactured weapons, the weapons are masterwork and gain an enhancement bonus (or equivalent weapon special abilities) when wielded by it. The bonus is based on its HD, as noted in the following table. A trompe l’oeil’s weapons melt into puddles of nonmagical paint when the creature is destroyed.</p>\n<hr>\n<table>\n<tbody>\n<tr>\n<td><strong>Trompe l'Oeil HD</strong></td>\n<td><strong>Weapon Enhancement Bonus</strong></td>\n</tr>\n<tr>\n<td>1-3</td>\n<td>—</td>\n</tr>\n<tr>\n<td>4-6</td>\n<td>+1</td>\n</tr>\n<tr>\n<td>7-9</td>\n<td>+2</td>\n</tr>\n<tr>\n<td>10-12</td>\n<td>+3</td>\n</tr>\n<tr>\n<td>13-15</td>\n<td>+4</td>\n</tr>\n<tr>\n<td>16+</td>\n<td>+5</td>\n</tr>\n</tbody>\n</table>\n<p><br><strong>Abilities:</strong> A trompe l’oeil has no Constitution score.<p><strong>Skills:</strong> A trompe l’oeil gains a +10 racial bonus on Disguise checks to appear as the base creature. It also receives a +5 bonus on Bluff checks to pretend to be the base creature and a +5 bonus on Stealth checks to appear as part of a painting.<p><strong>Special Qualities:</strong> A trompe l’oeil gains the following special qualities.<p><em>Autotelic (Ex):</em> A trompe l’oeil uses its Charisma score in place of its Constitution score when calculating hit points, Fortitude saves, and any special ability that relies on Constitution (such as when calculating a breath weapon’s DC).<p><em>Enter Painting (Su):</em> As a standard action, a trompe l’oeil can enter a painting it touches. When it does so, its physical body disappears, and its image appears in the painting. The trompe l’oeil can use its normal senses and attempt Perception checks to notice anything occurring near the painting. While within a painting, the trompe l’oeil can talk and move anywhere within the picture or even temporarily alter it (such as by picking a flower in the painting). It cannot use any spells or other abilities while within an image. In addition, the trompe l’oeil gains the freeze universal monster ability to appear as part of the painting. The trompe l’oeil can leave the painting as a move action. Once it leaves the painting, the image immediately reverts to the appearance it had before the trompe l’oeil entered. If someone destroys or damages the painting, the trompe l’oeil is unharmed, but exits the image.</p>"
    },
    "tags": [],
    "actions": [],
    "attackNotes": [],
    "effectNotes": [],
    "changes": [
      {
        "_id": "d4pzl47v",
        "formula": "max(0, ceil(@attributes.hd.total/4 - 1))",
        "operator": "add",
        "subTarget": "aac",
        "modifier": "untyped",
        "priority": 0,
        "value": 0,
        "target": "ac"
      },
      {
        "_id": "19d9kfhn",
        "formula": "(@attributes.hd.total >= 17) ? 2 : ((@attributes.hd.total >= 9 && @attributes.hd.total < 17) ? 1 : 0)",
        "operator": "add",
        "subTarget": "sac",
        "modifier": "untyped",
        "priority": 0,
        "value": 0,
        "target": "ac"
      }
    ],
    "contextNotes": [],
    "links": {
      "children": [],
      "charges": []
    },
    "armorProf": {
      "value": []
    },
    "weaponProf": {
      "value": []
    },
    "languages": {
      "value": []
    },
    "scriptCalls": [],
    "subType": "template",
    "associations": {
      "classes": []
    },
    "crOffset": "1"
  }
}
